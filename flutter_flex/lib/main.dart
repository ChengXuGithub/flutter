import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: IndexedStack(
        index: 0,
        children: [
          Positioned(
              left: 50,
              top: 50,
              right: 50,
              bottom: 50,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.red,
              )),
          Positioned(
              left: 150,
              top: 150,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.yellow,
              )),
        ],
      ),
    );
  }
}

class WrapWidget extends StatelessWidget {
  const WrapWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.spaceAround,
      runAlignment: WrapAlignment.spaceAround,
      textDirection: TextDirection.rtl,
      children: [
        Container(
          width: 100,
          height: 100,
          color: Colors.red,
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.yellow,
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.brown,
        ),
      ],
    );
  }
}

class FlexLayoutWidget extends StatelessWidget {
  const FlexLayoutWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      //direction: Axis.vertical, //主轴是垂直方向 对应的副轴(交叉轴)是水平方向
      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      verticalDirection: VerticalDirection.down,
      children: [
        Expanded(
            flex: 1,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.red,
            )),
        Expanded(
            flex: 1,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.yellow,
            )),
        Expanded(
            flex: 2,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            )),
      ],
    );
  }
}
