import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopping_cart/model/product.dart';
import 'package:shopping_cart/model/shppping_cart.dart';

class ShoppingCartPage extends StatefulWidget {
  ShoppingCartPage({Key? key}) : super(key: key);

  @override
  State<ShoppingCartPage> createState() => _ShoppingCartPageState();
}

class _ShoppingCartPageState extends State<ShoppingCartPage> {
  @override
  Widget build(BuildContext context) {
    // List<Product> list = Provider.of<ShoppingCart>(context).cart;
    List<Product> list = context.watch<ShoppingCart>().cart;
    return Scaffold(
      appBar: AppBar(
        title: const Text('购物车'),
      ),
      body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ListTile(
              leading: Image.network(
                'http://www.tangguoketang.com/img/banner01.7c3681ef.png',
                width: 100,
              ),
              title: Text('${list[index].name}'),
              trailing: IconButton(
                icon: Icon(Icons.remove),
                onPressed: () {
                  Provider.of<ShoppingCart>(context, listen: false)
                      .remove(list[index]);
                },
              ),
            );
          }),
    );
  }
}
