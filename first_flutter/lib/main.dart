import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('首页'),
        ),
        body: MyImage(),
      ),
    );
  }
}

//image
class MyImage extends StatefulWidget {
  MyImage({Key? key}) : super(key: key);

  @override
  State<MyImage> createState() => _MyImageState();
}

class _MyImageState extends State<MyImage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.network(
          'http://www.tangguoketang.com/img/banner01.7c3681ef.png',
          fit: BoxFit.cover,
        ),
        Image.asset('images/logo.png'),
      ],
    );
  }
}

//button
class MyButton extends StatefulWidget {
  MyButton({Key? key}) : super(key: key);

  @override
  State<MyButton> createState() => _MyButtonState();
}

class _MyButtonState extends State<MyButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 200,
        child: ElevatedButton(
          child: Text('press me '),
          onPressed: () {},
        ));
  }
}

//text
class MyHome extends StatefulWidget {
  MyHome({Key? key}) : super(key: key);

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      color: Colors.red,
      child: Text(
        'tangguodalong',
        style: TextStyle(
          fontSize: 30,
          fontStyle: FontStyle.italic,
          backgroundColor: Colors.blue,
          decoration: TextDecoration.underline,
        ),
        textAlign: TextAlign.right,
        maxLines: 1,
        overflow: TextOverflow.clip,
      ),
    );
  }
}
